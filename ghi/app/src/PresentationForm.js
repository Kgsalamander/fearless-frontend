import React, { useEffect, useState } from 'react';

function PresentaionForm () {
  //useState hooks
  const [presenter, setPresenter] = useState('')
  const [company, setCompany] = useState('')
  const [email, setEmail] = useState('')
  const [title, setTitle] = useState('')
  const [synopsis, setSynonpsis] = useState('')
  const [conference, setConference] = useState('')
  const [conferences, setConferences] = useState([])
  const [status, setStatus] = useState('')

  //handleChange Functions
  const handleName = (event) => {
    const value = event.target.value
    setPresenter(value)
  }
  const handleCompany = (event) => {
    const value = event.target.value
    setCompany(value)
  }
  const handleEmail = (event) => {
    const value = event.target.value
    setEmail(value)
  }
  const handleTitle = (event) => {
    const value = event.target.value
    setTitle(value)
  }
  const handleSynonpsis = (event) => {
    const value = event.target.value
    setSynonpsis(value)
  }
  const handleConference = (event) => {
    const value = event.target.value
    setConference(value)
  }


  //On Submit Form
  const handleSubmit = async (event) => {
    event.preventDefault()

    const data = {}
    data.presenter_name = presenter
    data.company_name = company
    data.presenter_email = email
    data.title = title
    data.synopsis = synopsis
    data.conference = conference

    const presentUrl = `http://localhost:8000/${conference}presentations/`;
      const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
          'Content-Type': 'application/json',
        },
      };

    const response = await fetch(presentUrl, fetchConfig);
      if (response.ok) {
        const newPresentation = await response.json();
        console.log(newPresentation);

        setPresenter('')
        setCompany('')
        setEmail('')
        setCompany('')
        setTitle('')
        setSynonpsis('')
        setConference('')
      }
  }
  //End Submit Form

  //Loads Json Data
  const fetchData = async () => {
    const url = `http://localhost:8000/api/conferences/`;

    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setConferences(data.conferences)
      }
    }
    useEffect(() => {
        fetchData();
    }, []);

  return (
    <>
    <div className="row">
    <div className="offset-3 col-6">
      <div className="shadow p-4 mt-4">
        <h1>Create a new presentation</h1>
        <form onSubmit={handleSubmit} id="create-presentation-form">
          <div className="form-floating mb-3">
            <input onChange={handleName} value={presenter} placeholder="Presenter name" required type="text" name="presenter_name" id="presenter_name" className="form-control"/>
            <label htmlFor="presenter_name">Presenter name</label>
          </div>
          <div className="form-floating mb-3">
            <input onChange={handleEmail} value={email} placeholder="Presenter email" required type="email" name="presenter_email" id="presenter_email" className="form-control"/>
            <label htmlFor="presenter_email">Presenter email</label>
          </div>
          <div className="form-floating mb-3">
            <input onChange={handleCompany} value={company} placeholder="Company name" type="text" name="company_name" id="company_name" className="form-control"/>
            <label htmlFor="company_name">Company name</label>
          </div>
          <div className="form-floating mb-3">
            <input onChange={handleTitle} value={title} placeholder="Title" required type="text" name="title" id="title" className="form-control"/>
            <label htmlFor="title">Title</label>
          </div>
          <div className="mb-3">
            <label htmlFor="synopsis">Synopsis</label>
            <textarea onChange={handleSynonpsis} value={synopsis} id="synopsis" rows="3" name="synopsis" className="form-control"></textarea>
          </div>
          <div className="mb-3">
            <select onChange={handleConference} value={conference} required name="conference" id="conference" className="form-select">
              <option value="">Choose a conference</option>
              {conferences.map(conference => {
                        return (
                             <option key={conference.href} value={conference.href}>
                                {conference.name}
                             </option>
                             )
                        })}
            </select>
          </div>
          <button className="btn btn-primary">Create</button>
        </form>
      </div>
    </div>
  </div>
  </>
  )
}
export default PresentaionForm

import React, { useEffect, useState } from 'react';

function ConferenceForm () {
  // useState hooks here
    const [locations, setLocations] = useState([])
    const [name, setName] = useState('')
    const [description, setDescription] = useState('')
    const [start, setStart] = useState('')
    const [end, setEnd] = useState('')
    const [presentations, setPresentations] = useState('')
    const [attendees, setAttendees] = useState('')
    const [location, setLocation] = useState('')

  // handleChange functions here
    const handleNameChange = (event) => {
      const value = event.target.value
      setName(value)
    }
    const handleDescription = (event)=> {
      const value = event.target.value
      setDescription(value)
    }
    const handleStarts = (event) => {
      const value = event.target.value
      setStart(value)
    }
    const handleEnds = (event) => {
      const value = event.target.value
      setEnd(value)
    }
    const handlePresentaions = (event) => {
      const value = event.target.value
      setPresentations(value)
    }
    const handleAttendees = (event) => {
      const value = event.target.value
      setAttendees(value)
    }
    const handleLocation = (event) => {
      const value = event.target.value
      setLocation(value)
    }

  // On Submit of Form
    const handleSubmit = async (event) => {
      event.preventDefault();

      const data = {};
      data.name = name
      data.description = description
      data.starts = start
      data.ends = end
      data.max_presentations = presentations
      data.max_attendees = attendees
      data.location = location

      const conferenceUrl = 'http://localhost:8000/api/conferences/';
      const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
          'Content-Type': 'application/json',
        },
      };

      const response = await fetch(conferenceUrl, fetchConfig);
      if (response.ok) {
        const newConference = await response.json();
        console.log(newConference);

        setName('')
        setDescription('')
        setStart('')
        setEnd('')
        setPresentations('')
        setAttendees('')
        setLocation('')
      }
    }
  // End of form Submit




  //loads json data
    const fetchData = async () => {
        const url = 'http://localhost:8000/api/locations/';

        const response = await fetch(url);
        if (response.ok) {
          const data = await response.json();
          setLocations(data.locations)
          }
        }
        useEffect(() => {
            fetchData();
        }, []);
    //end Data load

    return (
        <>
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new conference</h1>
            <div id="error"></div>
            <form onSubmit={handleSubmit} id="create-conference-form">
              <div className="form-floating mb-3">
                <input onChange={handleNameChange} value={name} placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
                <label htmlFor="name">Name</label>
              </div>
              <div className="form mb-3">
                <label htmlFor="description">Descrition</label>
                <textarea onChange={handleDescription} value={description} placeholder="Description" required type="text" name="description" id="description" rows="3" className="form-control"></textarea>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleStarts} value={start} placeholder="Starts" required type="date" name="starts" id="starts" className="form-control"/>
                <label htmlFor="starts">Starts</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleEnds} value={end} placeholder="Ends" required type="date" name="ends" id="ends" className="form-control"/>
                <label htmlFor="ends">Ends</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handlePresentaions} value={presentations} placeholder="Max Presentation" required type="number" name="max_presentations" id="max_presentations" className="form-control"/>
                <label htmlFor="max_presentations">Max presentations</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleAttendees} value={attendees} placeholder="Max Attendees" required type="number" name="max_attendees" id="max_attendees" className="form-control"/>
                <label htmlFor="max_attendees">Max attendees</label>
              </div>
              <div className="mb-3">
                <select onChange={handleLocation} value={location} required id="location" name="location" className="form-select">
                  <option value="">Choose a Location</option>
                  {locations.map(location => {
                        return (
                             <option key={location.id} value={location.id}>
                                {location.name}
                             </option>
                             )
                        })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
      </>
    )
}

export default ConferenceForm;

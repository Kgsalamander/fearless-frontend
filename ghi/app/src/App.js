import Nav from './Nav';
import MainPage from './MainPage';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import AttendConferenceForm from './AttendConferenceForm';
import PresentaionForm from './PresentationForm';
import {
  BrowserRouter,
  createBrowserRouter,
  RouterProvider,
  Route,
  Routes,
} from "react-router-dom";

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
    <Nav />
    <div className="container">
      <Routes>
        <Route index element={<MainPage />} />
        <Route path='locations'>
          <Route path='new' element={<LocationForm />}/>
        </Route>
        <Route path='attendConference'>
          <Route path='new' element={<AttendConferenceForm />}/>
        </Route>
        <Route path='conferences'>
          <Route path='new' element={<ConferenceForm />}/>
        </Route>
        <Route path='attendees'>
          <Route path='new' element={<AttendeesList attendees={props.attendees} />}/>
        </Route>
        <Route path='presentations'>
          <Route path='new' element={<PresentaionForm />}/>
        </Route>
      </Routes>
    </div>
    </BrowserRouter>
  );
}
export default App

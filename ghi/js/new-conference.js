window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/locations/'

    const formTag = document.getElementById('create-conference-form')
    formTag.addEventListener('submit',  async event => {
        event.preventDefault()
        const formData = new FormData(formTag)
        const json = JSON.stringify(Object.fromEntries(formData))

        const conferenceUrl = 'http://localhost:8000/api/conferences/'
        const fetchConfig ={
            method: "POST",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        }
        const response = await fetch(conferenceUrl, fetchConfig)
        if (response.ok){
            formTag.reset()
            const newConference = await response.json()
            console.log(newConference)
        }
    })

    function causePanic(e){
        return `
        <div class="alert alert-warning d-flex align-items-center" role="alert">
        <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Warning:"><use xlink:href="#exclamation-triangle-fill"/></svg>
        <div>
        Im Sorry, There was a problem loading this page.
        Error: ${e}
        </div>
        </div>
        `
    }

    try {

        const response = await fetch(url)
        if (!response.ok) {
            throw new Error('Response is not daijobu')
        }
        else {
            const data = await response.json()
            //the forloop didnt work, then fixed itself, dont mess with it anymore
            const selectTag = document.getElementById('location')
            for (let location of data.locations) {
                const option = document.createElement('option')
                option.value = location.id
                option.innerHTML = location.name
                selectTag.appendChild(option)
            }
        }
    }
    catch (e) {
        console.error(e)
        const panic = causePanic(e)
        const errorMessage = document.querySelector('#error')
        errorMessage.innerHTML += panic
    }
})

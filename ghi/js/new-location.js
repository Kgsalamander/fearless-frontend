window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/states/'

    const formTag = document.getElementById('create-location-form')
    formTag.addEventListener('submit',  async event => {
        event.preventDefault()
        const formData = new FormData(formTag)
        const json = JSON.stringify(Object.fromEntries(formData))

        const locationUrl = 'http://localhost:8000/api/locations/'
        const fetchConfig ={
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        }
        const response = await fetch(locationUrl, fetchConfig)
        if (response.ok){
            formTag.reset()
            const newLocation = await response.json()
            console.log(newLocation)
        }
    })

    function causePanic(e){
        return `
        <div class="alert alert-warning d-flex align-items-center" role="alert">
        <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Warning:"><use xlink:href="#exclamation-triangle-fill"/></svg>
        <div>
        Im Sorry, There was a problem loading this page.
        Error: ${e}
        </div>
        </div>
        `
    }

    try {

        const response = await fetch(url)
        if (!response.ok) {
            throw new Error('Response is not daijobu')
        }
        else {
            const data = await response.json()
            //the forloop didnt work, then fixed itself, dont mess with it anymore
            console.log(data)
            const selectTag = document.getElementById('state')
            for (let state of data.states) {
                const option = document.createElement('option')
                option.value = state.abbreviation
                option.innerHTML = state.name
                selectTag.appendChild(option)
            }
        }
    }
    catch (e) {
        console.error(e)
        const panic = causePanic(e)
        const errorMessage = document.querySelector('#error')
        errorMessage.innerHTML += panic
    }
})

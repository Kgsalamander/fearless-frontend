window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    function createCard(name, description, pictureUrl, starts, ends, location) {
        return `
          <div class="card shadow-lg p-3 mb-5 bg-body-tertiary rounded ms-5">
            <img src="${pictureUrl}" class="card-img-top">
            <div class="card-body">
              <h5 class="card-title">${name}</h5>
              <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
              <p class="card-text">${description}</p>
              <a href="#" class="btn btn-primary">Go somewhere</a>
            </div>
            <div class="card-footer text-muted">
             starts ${starts} - ends ${ends}
            </div>
          </div>
        `;
      }

    function causePanic(e){
      return `
      <div class="alert alert-warning d-flex align-items-center" role="alert">
      <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Warning:"><use xlink:href="#exclamation-triangle-fill"/></svg>
        <div>
          Im Sorry, There was a problem loading this page.
          Error: ${e}
        </div>
      </div>
      `;
    }

    try {
      const response = await fetch(url);

      if (!response.ok) {
        throw new Error('Response not ok')
      } else {
        const data = await response.json();

        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const title = details.conference.name;
            const location = details.conference.location.name;
            const description = details.conference.description;
            const starts = new Date(details.conference.starts).toLocaleDateString();
            const ends = new Date(details.conference.ends).toLocaleDateString();
            const pictureUrl = details.conference.location.picture_url;
            const html = createCard(title, description, pictureUrl, starts, ends, location);
            const column = document.querySelector('.col')
            column.innerHTML += html
          }
        }

      }
    } catch (e) {
        console.error(e);
        const panic = causePanic(e)
        const errorMessage = document.querySelector('#error')
        errorMessage.innerHTML += panic
      // Figure out what to do if an error is raised
    }

  });
